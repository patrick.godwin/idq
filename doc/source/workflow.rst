.. _workflow-workflow:

Workflow
####################################################################################################

**WRITE ME**

.. _workflow-batch:

Batch
====================================================================================================

.. graphviz::

   digraph idq_batch {
      labeljust = "r";
      label="idq-batch"
      rankdir=TB;
      graph [fontname="helvetica", fontsize=24];
      edge [ fontname="helvetica", fontsize=10 ];
      node [fontname="helvetica", shape=box, fontsize=11];
      style=rounded;
      labeljust = "r";
      fontsize = 14;


      DataSrc [label="auxiliary features"];

      Train [label="batch.train"];
      Evaluate [label="batch.evaluate"];
      Calibrate [label="batch.calibrate"];
      Timeseries [label="batch.timeseries"];

      DataSrc -> Train;
      Train -> Evaluate [label="model"];

      DataSrc -> Evaluate;
      Evaluate -> Calibrate [label="quiver"];

      DataSrc -> Timeseries;
      Train -> Timeseries [label="model"];
      Calibrate -> Timeseries [label="calibration map"];

      PGlitch [label="p(glitch)"]

      Timeseries -> PGlitch;

   }

TODO:

* Describe flow chart in more detail
* Describe how data will be processed
* Describe how distributed processes will communicate with one another.
* Add round-robin based training workflow
    
Stream
====================================================================================================

.. graphviz::

   digraph idq_streaming {
      labeljust = "r";
      label="idq-stream"
      rankdir=TB;
      graph [fontname="helvetica", fontsize=24];
      edge [ fontname="helvetica", fontsize=10 ];
      node [fontname="helvetica", shape=box, fontsize=11];
      style=rounded;
      labeljust = "r";
      fontsize = 14;


      DataSrc [label="auxiliary features"];

      Train [label="stream.train"];
      Evaluate [label="stream.evaluate"];
      Calibrate [label="stream.calibrate"];
      Timeseries [label="stream.timeseries"];

      DataSrc -> Train;
      Train -> Evaluate [label="model"];

      DataSrc -> Evaluate;
      Evaluate -> Calibrate [label="quiver"];

      DataSrc -> Timeseries;
      Train -> Timeseries [label="model"];
      Calibrate -> Timeseries [label="calibration map"];

      PGlitch [label="p(glitch)"]

      Timeseries -> PGlitch;

   }

.. _workflow-batch-vs-stream:

Batch vs Stream modes
====================================================================================================

**WRITE ME**

.. _workflow-tasks:

Tasks
====================================================================================================

**WRITE ME**

.. _workflow-training:

Training
----------------------------------------------------------------------------------------------------

**WRITE ME**

.. _workflow-evaluation:

Evaluation
----------------------------------------------------------------------------------------------------

**WRITE ME**

.. _workflow-calibration:

Calibration
----------------------------------------------------------------------------------------------------

**WRITE ME**

.. _workflow-timeseries:

Timeseries
----------------------------------------------------------------------------------------------------

**WRITE ME**
