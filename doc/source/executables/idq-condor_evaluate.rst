.. _idq-condor_evaluate:

idq-condor_evaluate
####################################################################################################

describe script, which modules it relies upon, etc.

.. program-output:: idq-condor_evaluate --help
   :nostderr:
