.. _idq-timeseries:

idq-timeseries
####################################################################################################

describe script, which modules it relies upon, etc

.. graphviz::

   digraph idq_timeseries {
      labeljust = "r";
      label=""
      rankdir=LR;
      graph [fontname="Roman", fontsize=24];
      edge [ fontname="Roman", fontsize=10 ];
      node [fontname="Roman", shape=box, fontsize=11];
      style=rounded;
      labeljust = "r";
      fontsize = 14;


      DataSrc [label="auxiliary features"];
      Model [label="model"]
      CalibMap [label="calibration map"];
      Timeseries [label="idq-timeseries"];
      PGlitch [label="p(glitch) timeseries"];

      DataSrc -> Timeseries;
      Model -> Timeseries;
      CalibMap -> Timeseries;
      Timeseries -> PGlitch;

   }

.. program-output:: idq-timeseries --help
   :nostderr:
