.. _executables:

Executables
####################################################################################################

`iDQ` accomplishes its decomposition of the inference problem through several asynchronous processes

* (re-)training,
* evaluation and (re-)calibration, and
* timeseries generation.

These are managed through a set of executables

Batch executables
----------------------------------------------------------------------------------------------------

These executables manage one-off batch jobs

.. toctree::
    :maxdepth: 1

    idq-batch
    idq-train
    idq-evaluate
    idq-calibrate
    idq-timeseries
    idq-report

Stream executables
----------------------------------------------------------------------------------------------------

These executables manage streaming processes, which can persist for long periods of time

.. toctree::
    :maxdepth: 1

    idq-stream
    idq-streaming_train
    idq-streaming_evaluate
    idq-streaming_calibrate
    idq-streaming_timeseries

Condor executables
----------------------------------------------------------------------------------------------------

These executables are helpers that allow for parallelization via Condor

.. toctree::
    :maxdepth: 1

    idq-condor_train
    idq-condor_evaluate
    idq-condor_calibrate
    idq-condor_timeseries

Misc executables
----------------------------------------------------------------------------------------------------

The following are generally useful tools but not required for the main pipeline

.. toctree::
    :maxdepth: 1

    idq-simtimeseries
    idq-check_config
    idq-kwm2kws
