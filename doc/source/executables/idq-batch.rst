.. _idq-batch:

idq-batch
####################################################################################################

describe script, which modules it relies upon, etc

.. graphviz::

   digraph idq_batch {
      labeljust = "r";
      label=""
      rankdir=LR;
      graph [fontname="Roman", fontsize=24];
      edge [ fontname="Roman", fontsize=10 ];
      node [fontname="Roman", shape=box, fontsize=11];
      style=rounded;
      labeljust = "r";
      fontsize = 14;


      DataSrc [label="auxiliary features"];

      Model [label="model"]
      Quiver [label="quiver"];
      CalibMap [label="calibration map"];

      Train [label="idq-train"];
      Evaluate [label="idq-evaluate"];
      Calibrate [label="idq-calibrate"];
      Timeseries [label="idq-timeseries"];

      PGlitch [label="p(glitch) timeseries"];

      DataSrc -> Train;
      Train -> Model;

      DataSrc -> Evaluate;
      Model -> Evaluate;
      Evaluate -> Quiver;

      Quiver -> Calibrate;
      Calibrate -> CalibMap;

      DataSrc -> Timeseries;
      Model -> Timeseries;
      CalibMap -> Timeseries;
      Timeseries -> PGlitch;

   }

.. program-output:: idq-batch --help
   :nostderr:
