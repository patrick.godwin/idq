.. _idq-check_config:

idq-check_config
####################################################################################################

describe script, which modules it relies upon, etc.

.. program-output:: idq-check_config --help
   :nostderr:
