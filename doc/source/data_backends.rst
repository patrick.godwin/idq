.. _data_backends-workflow:

Feature Backends
####################################################################################################

The workhorse that enables data discovery to find features is the :class:`idq.io.triggers.DataLoader`.
One can instantiate a flavor of :class:`idq.io.triggers.DataLoader` explicitly to acquire features
or configure one through a configuration file as part of the streaming or batch workflows.

.. autoclass:: idq.io.triggers.DataLoader
    :members:

.. _io-omicron_classifierdata:

Omicron-based
----------------------------------------------------------------------------------------------------

.. autoclass:: idq.io.triggers.omicron.OmicronDataLoader

**Keyword arguments:**

The following keyword arguments are required:

* **instrument:** the instrument for which features are derived from

In addition, the following optional keyword arguments can be passed in:

* **skip_bad_files:** allows one to skip over problematic files with incorrect permissions

.. _io-snax_dataloader:

SNAX-based
----------------------------------------------------------------------------------------------------

.. autoclass:: idq.io.triggers.snax.SNAXDataLoader

**Keyword arguments:**

The following keyword arguments are required:

* **rootdir:** base directory where features are located
* **basename:** name of files/directories containing GstLAL-based features

.. autoclass:: idq.io.triggers.snax.SNAXKafkaDataLoader

**Keyword arguments:**

The following keyword arguments are required:

* **group:** the Kafka consumer group to subscribe to
* **port:** the Kafka port to subscribe to
* **topic:** the Kafka topic to subscribe to
* **poll_timeout:** how long to wait for a message before timing out
* **retry_cadence:** how long to wait between retries
* **sample_rate:** the sampling rate of incoming features

.. _io-kw_dataloader:

Kleine-Welle-based
----------------------------------------------------------------------------------------------------

.. autoclass:: idq.io.triggers.kw.KWDataLoader

**Keyword arguments:**

The following keyword arguments are required:

* **instrument:** the instrument for which features are derived from
