[metadata]
name = iDQ
author = Reed Essick
author_email = reed.essick@ligo.org
maintainer = Patrick Godwin
maintainer_email = patrick.godwin@ligo.org
description = A low-latency statistical data quality pipeline for glitch detection
long_description = file: README.md
long_description_content_type = text/markdown
license = MIT
license_files = LICENSE
url = https://git.ligo.org/lscsoft/iDQ
classifiers =
	Development Status :: 3 - Alpha
	Programming Language :: Python
	Programming Language :: Python :: 3
	Intended Audience :: Science/Research
	Intended Audience :: End Users/Desktop
	Intended Audience :: Developers
	Natural Language :: English
	Topic :: Scientific/Engineering
	Topic :: Scientific/Engineering :: Astronomy
	Topic :: Scientific/Engineering :: Physics
	Operating System :: POSIX
	Operating System :: Unix
	Operating System :: MacOS
	License :: OSI Approved :: MIT License
project_urls =
	Bug Tracker = https://git.ligo.org/lscsoft/iDQ/issues
	Documentation = https://lscsoft.docs.ligo.org/iDQ
	Source Code = https://git.ligo.org/lscsoft/iDQ.git

[options]
packages = find:
scripts =
	bin/idq-batch
	bin/idq-train
	bin/idq-calibrate
	bin/idq-evaluate
	bin/idq-timeseries
	bin/idq-report
	bin/idq-condor_batch
	bin/idq-stream
	bin/idq-streaming_train
	bin/idq-streaming_evaluate
	bin/idq-streaming_calibrate
	bin/idq-streaming_timeseries
	bin/idq-streaming_report
	bin/idq-smpull
	bin/idq-create-channel-list
	bin/idq-generate-vetoes
	bin/idq-target-times
	bin/idq-check_config
	bin/idq-simtimeseries
zip_safe = False
python_requires = >=3.9
setup_requires =
	setuptools >= 38.2.5
	setuptools-scm
	wheel
install_requires =
	dqsegdb2 >= 1.0
	ezdag
	gpstime
	gwpy >= 2.1
	gwtrigfind
	h5py >= 2.2.1
	ligo-scald >= 0.8
	ligo-segments >= 1.0.0
	matplotlib >= 2.0
	numpy >= 1.7.1
	pluggy
	scipy
	toml
	ujson

[options.extras_require]
docs =
	docutils < 0.17
	graphviz
	sphinx
	sphinx_rtd_theme == 0.4.0
	sphinxcontrib-programoutput
test =
	pytest >= 5
	pytest-console-scripts
	pytest-cov
dev =
	%(docs)s
	%(test)s
	black
	flake8
	flake8-black

[flake8]
max-line-length = 88
extend-ignore =
	# See https://github.com/PyCQA/pycodestyle/issues/373
	E203,
per-file-ignores =
	__init__.py:F401
	_version.py:BLK100
	# ignore filter warnings on tensorflow import
	idq/classifiers/keras.py:E402
