import pickle

from ... import hookimpl
from . import DiskReporter


class PickleReporter(DiskReporter):
    """
    pickle objects instead of just writing strings into plaintxt
    """

    _suffix = "pkl"

    def _write(self, path, obj, **kwargs):
        """
        pickles obj into path
        """
        with open(path, "wb") as file_obj:
            pickle.dump(obj, file_obj)

    @classmethod
    def read(cls, path):
        """
        unpickles obj from self.path(nickname)
        """
        with open(path, "rb") as file_obj:
            obj = pickle.load(file_obj)
        return obj


@hookimpl
def get_reporters():
    return {"pickle": PickleReporter}
