#!/usr/bin/env python

__usage__ = "sanitycheck_mockclassifierdata [--options] mcd_config.ini"
__doc__ = "a basic script to test MockClassifierData functionality"
__author__ = "reed.essick@ligo.org"

#-------------------------------------------------

import numpy as np
from optparse import OptionParser

import pickle

### non-standard libraries
from idq import io

#-------------------------------------------------

parser = OptionParser(usage=__usage__, description=__doc__)

parser.add_option('-v', '--verbose', default=False, action='store_true')

parser.add_option('-s', '--gps-start', default=0, type='float')
parser.add_option('-e', '--gps-end', default=100., type='float')

opts, args = parser.parse_args()
assert len(args)==1, 'please supply exactly 1 input argument\n%s'%__usage__
mcd_config = args[0]

#-------------------------------------------------

if opts.verbose:
    print('instantiating MockClassifierData object')
cd = io.MockClassifierData(opts.gps_start, opts.gps_end, config=mcd_config)

if opts.verbose:
    print('testing trigger generation')
trgs = cd.triggers()
trgs2 = cd.triggers()
assert np.all(trgs.keys()==trgs2.keys())
for chan in trgs.keys():
    assert np.all(trgs[chan]==trgs2[chan])

if opts.verbose:
    print('testing pickle-ability')
pickled = pickle.dumps(cd)
cd2 = pickle.loads(pickled)

trgs3 = cd2.triggers()
assert np.all(trgs.keys()==trgs3.keys())
for chan in trgs.keys():
    assert np.all(trgs[chan]==trgs3[chan])
