#!/usr/bin/env python

__usage__ = "sanitycheck_kwsclassifierdata [--options] start end"
__doc__ = "basic testing for idq.io.KWMClassifierData"
__author__ = "reed.essick@ligo.org"

#-------------------------------------------------

from idq import io

import time

from optparse import OptionParser

#-------------------------------------------------

parser = OptionParser(usage=__usage__, description=__doc__)

parser.add_option('-v', '--verbose', default=False, action='store_true')
parser.add_option('-V', '--Verbose', default=False, action='store_true')

parser.add_option('', '--channel', default=[], type='string', action='append')

parser.add_option('', '--rootdir', default='/gds-l1/dmt/triggers/L-KW_TRIGGERS/', type='string')
parser.add_option('', '--stride', default=None, type='int',
    help='if provided, use PredictiveKWSClassifierData instead of KWSClassifierData')

opts, args = parser.parse_args()
assert len(args)==2, 'please supply exactly 2 input arguments\n%s'%__usage__
start = float(args[0])
end = float(args[1])

opts.verbose |= opts.Verbose

#-------------------------------------------------

if opts.verbose:
    print('instantiating object')
if opts.stride:
    kws = io.PredictiveKWSClassifierData(
        start,
        end,
        rootdir=opts.rootdir,
        stride=opts.stride,
    )

else:
    kws = io.KWSClassifierData(
        start,
        end,
        rootdir=opts.rootdir,
    )

#--- perform tests!
if opts.verbose:
    print('fetching triggers')
to = time.time()
trgs = kws.triggers(opts.channel, verbose=opts.Verbose)
print('%.3f sec for %d triggers from %d channels covering %.3f sec of data'%(time.time()-to, sum(len(_) for _ in trgs.values()), len(trgs.keys()), end-start))
