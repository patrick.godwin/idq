#!/usr/bin/env python

__usage__ = "sanitycheck_calibrate [--options] start end config.ini"
__doc__ = "basic sanity check for idq-calibrate"
__author__ = "reed.essick@ligo.org"

#-------------------------------------------------

import os

import numpy as np
from scipy.stats import norm
from scipy.stats import beta

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt

from ConfigParser import SafeConfigParser

from optparse import OptionParser

### non-standard libraries
from idq import batch
from idq import configparser
from idq import names
from idq import calibration

#-------------------------------------------------

parser = OptionParser(usage=__usage__, description=__doc__)

parser.add_option('-v', '--verbose', default=False, action='store_true')

parser.add_option('-o', '--output-dir', default='.', type='string')
parser.add_option('-t', '--tag', default='', type='string')

opts, args = parser.parse_args()
assert len(args)==3, 'please supply exactly 3 input arguments\n%s'%__usage__
start, end = [float(_) for _ in args[:2]]
config_path = args[2]

if not os.path.exists(opts.output_dir):
    os.makedirs(opts.output_dir)

if opts.tag:
    opts.tag = "_"+opts.tag

#-------------------------------------------------

### run calibration
batch.calibrate(start, end, config_path, verbose=opts.verbose)

### set up everything we need to load in data
if opts.verbose:
    print('reading config : '+config_path)
config = configparser.path2config(config_path)

tag = config.tag
rootdir = os.path.abspath(config.rootdir)
calibratedir = names.tag2calibratedir(tag, rootdir=rootdir)

calibratereporter = configparser.config2reporter(calibratedir, start, end, **config.calibrate['reporting'])

### plot results for each classifier
cumbins = np.linspace(0, 1, 1001)
ranks = np.linspace(0, 1, 101)
rocranks = np.logspace(-3, 0, 101)[::-1]

for nickname in config.classifiers:
    if opts.verbose:
        print('sanity checking results for : '+nickname)
    items = config.items(nickname)
    calibmap = calibratereporter.retrieve(nickname)

    gch_obs = calibmap._gch_kde.obs
    gbins = np.linspace(0, 1, max(15, int(len(gch_obs)**0.5)))
    cln_obs = calibmap._cln_kde.obs
    cbins = np.linspace(0, 1, max(15, int(len(cln_obs)**0.5)))

    ### plot results
    # pdf
    if opts.verbose:
        print('plotting pdf estimates')
    fig = plt.figure()
    ax = fig.gca()

    for q in [0.1, 0.2, 0.3, 0.4]:
        ax.fill_between(ranks, calibmap.gch_pdf_quantile(ranks, q), calibmap.gch_pdf_quantile(ranks, 1.-q), color='r', alpha=0.10)
    ax.plot(ranks, calibmap.gch_pdf(ranks), color='r', label='p(r|g)')
    ax.plot(ranks, [beta.median(a,b)*3/(2*np.pi*calibmap._gch_kde.b**2)**0.5 for a, b in zip(calibmap._gch_kde.pdf_alpha(ranks), calibmap._gch_kde.pdf_beta(ranks))], color='m', linestyle='dashed')
    ax.hist(gch_obs, bins=gbins, color='m', histtype='step', normed=True)

    for q in [0.1, 0.2, 0.3, 0.4]:
        ax.fill_between(ranks, calibmap.cln_pdf_quantile(ranks, q), calibmap.cln_pdf_quantile(ranks, 1.-q), color='b', alpha=0.10)
    ax.plot(ranks, calibmap.cln_pdf(ranks), color='b', label='p(r|c)')
    ax.plot(ranks, [beta.median(a,b)*3/(2*np.pi*calibmap._cln_kde.b**2)**0.5 for a, b in zip(calibmap._cln_kde.pdf_alpha(ranks), calibmap._cln_kde.pdf_beta(ranks))], color='c', linestyle='dashed')
    ax.hist(cln_obs, bins=cbins, color='c', histtype='step', normed=True)

    ax.set_xlabel('rank')
    ax.set_ylabel('pdf')

    ax.set_xlim(xmin=0, xmax=1)

    ax.grid(True, which='both')
    ax.legend(loc='best')

    figname = os.path.join(opts.output_dir, 'sanitycheck_calibrate-pdf-%s%s.png'%(nickname, opts.tag))
    if opts.verbose:
        print('    saving: '+figname)
    fig.savefig(figname)
    plt.close(fig)

    # cdf
    if opts.verbose:
        print('plotting cdf estimates')
    fig = plt.figure()
    ax = fig.gca()

    for q in [0.1, 0.2, 0.3, 0.4]:
        ax.fill_between(ranks, calibmap.gch_cdf_quantile(ranks, q), calibmap.gch_cdf_quantile(ranks, 1.-q), color='r', alpha=0.10)
    ax.plot(ranks, calibmap.gch_cdf(ranks), color='r', label='P(r|g)')
    ax.plot(ranks, [beta.median(a,b) for a, b in zip(calibmap._gch_kde.cdf_alpha(ranks), calibmap._gch_kde.cdf_beta(ranks))], color='m', linestyle='dashed')
    ax.hist(gch_obs, bins=cumbins, color='m', cumulative=1, histtype='step', normed=True)

    for q in [0.1, 0.2, 0.3, 0.4]:
        ax.fill_between(ranks, calibmap.cln_cdf_quantile(ranks, q), calibmap.cln_cdf_quantile(ranks, 1.-q), color='b', alpha=0.10)
    ax.plot(ranks, calibmap.cln_cdf(ranks), color='b', label='P(r|c)')
    ax.plot(ranks, [beta.median(a,b) for a, b in zip(calibmap._cln_kde.cdf_alpha(ranks), calibmap._cln_kde.cdf_beta(ranks))], color='c', linestyle='dashed')
    ax.hist(cln_obs, bins=cumbins, color='c', cumulative=1, histtype='step', normed=True)

    ax.set_xlabel('rank')
    ax.set_ylabel('cdf')

    ax.set_xlim(xmin=0, xmax=1)
    ax.set_ylim(ymin=0, ymax=1)

    ax.grid(True, which='both')
    ax.legend(loc='best')

    figname = os.path.join(opts.output_dir, 'sanitycheck_calibrate-cdf-%s%s.png'%(nickname, opts.tag))
    if opts.verbose:
        print('    saving: '+figname)
    fig.savefig(figname)
    plt.close(fig)

    # ROC
    if opts.verbose:
        print('plotting ROC estimates')
    fig = plt.figure()
    ax = fig.gca()

    fap, eff = calibmap.roc(rocranks)
    fap[-1] = 1 ### for numerical stability...
    eff[-1] = 1
    ax.loglog(fap, eff, color='b')

    f = np.zeros_like(rocranks, dtype='float')
    for o in cln_obs:
        f += o>=ranks
    f /= len(cln_obs)

    g = np.zeros_like(rocranks, dtype='float')
    for o in gch_obs:
        g += o>=ranks
    g /= len(gch_obs)

    ax.loglog(f, g, 'r')

    ax.set_xlabel('fap')
    ax.set_ylabel('eff')

    ax.set_xlim(xmax=1)
    ax.set_ylim(ymax=1)

    ax.grid(True, which='both')

    figname = os.path.join(opts.output_dir, 'sanitycheck_calibrate-roc-%s%s.png'%(nickname, opts.tag))
    if opts.verbose:
        print('    saving: '+figname)
    fig.savefig(figname)
    plt.close(fig)

    # loglike
    if opts.verbose:
        print('plotting loglike quantiles')
    fig = plt.figure()
    ax = fig.gca()

    loglike = calibmap.loglike(ranks)
    Eloglike = calibmap.loglike_mean(ranks)

    for rank, samples in zip(calibmap._ranks, calibmap._interp_loglike_cdf):
        ax.plot([rank]*len(samples), samples, linestyle='none', marker='o', markeredgecolor='none', markerfacecolor='grey', markersize=2, alpha=0.005)

    min_loglike = np.min(loglike)
    max_loglike = np.max(loglike)

    for q in np.arange(0, 1, 0.1)[1:]:
        quantile = calibmap.loglike_quantile(ranks, q)
        min_loglike = min(min_loglike, np.min(quantile))
        max_loglike = max(max_loglike, np.max(quantile))
        ax.plot(ranks, calibmap.loglike_quantile(ranks, q), 'k-', alpha=0.5)

    ax.plot(ranks, loglike, 'b-', linewidth=2, label='log(E[g]) - log(E[c])')
    ax.plot(ranks, Eloglike, 'r-', linewidth=2, label='E[log(g) - log(c)]')

    ax.set_xlabel('rank')
    ax.set_ylabel('log(likelihood)')

    ax.set_xlim(xmin=0, xmax=1)
    ax.set_ylim(ymin=min_loglike, ymax=max_loglike)

    ax.grid(True, which='both')
    ax.legend(loc='best')

    figname = os.path.join(opts.output_dir, 'sanitycheck_calibrate-logL-%s%s.png'%(nickname, opts.tag))
    if opts.verbose:
        print('    saving: '+figname)
    fig.savefig(figname)
    plt.close(fig)
