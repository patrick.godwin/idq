#!/usr/bin/env python

__usage__ = "sanitycheck_io-scaling [--options] start end"
__doc__ = "basic testing for testing how we construct quivers and how long they take to vectorize. This is based on KWMClassifierData for convenience"
__author__ = "reed.essick@ligo.org"

#-------------------------------------------------

import numpy as np

import time as TIME

from optparse import OptionParser

### non-standard libraries
from idq import io
from idq import features
from idq import factories

#-------------------------------------------------

parser = OptionParser(usage=__usage__, description=__doc__)

parser.add_option('-v', '--verbose', default=False, action='store_true')
parser.add_option('-V', '--Verbose', default=False, action='store_true')

parser.add_option('', '--channels', default='channels.txt', type='string')

parser.add_option('', '--kwm-rootdir', default='/gds-l1/dmt/triggers/L-KW_TRIGGERS/', type='string')
parser.add_option('', '--kwm-basename', default='L-KW_TRIGGERS', type='string')

parser.add_option('', '--stride', default=32, type='int')
parser.add_option('', '--window', default=0.1, type='float')
parser.add_option('', '--srate', default=0.01, type='float')

opts, args = parser.parse_args()
assert len(args)==2, 'please supply exactly 2 input arguments\n%s'%__usage__
start = float(args[0])
end = float(args[1])

opts.verbose |= opts.Verbose

time = 'time' ### column names
significance = 'significance'

default = -np.infty

times = np.arange(start, end, 1./opts.srate)

#-------------------------------------------------

if opts.verbose:
    print('instantiating KWM object')
    to = TIME.time()

kwm_nstride_nwindow = io.KWMClassifierData(
    start,
    end,
    rootdir=opts.kwm_rootdir,
    basename=opts.kwm_basename,
    columns=['time', 'significance', 'frequency'],
)

kwm_nstride_ywindow = io.KWMClassifierData(
    start,
    end,
    rootdir=opts.kwm_rootdir,
    basename=opts.kwm_basename,
    columns=['time', 'significance', 'frequency'],
    window=opts.window,
)

kwm_ystride_nwindow = io.PredictiveKWMClassifierData(
    start,
    end,
    rootdir=opts.kwm_rootdir,
    basename=opts.kwm_basename,
    columns=['time', 'significance', 'frequency'],
    stride=opts.stride,
    window=opts.window, ### needed to make stride take effect
    do_not_window=True, ### needed to stop windowing around vectors
)

kwm_ystride_ywindow = io.PredictiveKWMClassifierData(
    start,
    end,
    rootdir=opts.kwm_rootdir,
    basename=opts.kwm_basename,
    columns=['time', 'significance', 'frequency'],
    stride=opts.stride,
    window=opts.window,
    do_not_window=False,
)

if opts.verbose:
    print('    %.6f sec'%(TIME.time()-to))

#---

if opts.verbose:
    print('setting up vectorize arguments')
    to = TIME.time()

channels = io.path2channels(opts.channels)
select = features.SelectLoudest(
    default,
    window=opts.window,
    time=time,
    significance=significance,
)

if opts.verbose:
    print('    %.6f sec'%(TIME.time()-to))

#---

for kwm, name in [
        (kwm_nstride_nwindow, 'no stride; no window'),
        (kwm_nstride_ywindow, 'no stride; yes window'),
        (kwm_ystride_nwindow, 'yes stride; no window'),
        (kwm_ystride_ywindow, 'yes stride; yes window'),
    ]:
    if opts.verbose:
        print('working on : '+name)
        print('    instantiating quiver')
        to = TIME.time()

    quiver = factories.QuiverFactory(kwm)(times)

    if opts.verbose:
        print('        %.6f sec'%(TIME.time()-to))
        print('    quiver contains %d vectors'%len(quiver))

    if opts.verbose:
        print('    vectorizing')
        to = TIME.time()

    d, n = quiver.vectorize(
        channels, 
        select, 
    )

    if opts.verbose:
        print('        %.6f sec'%(TIME.time()-to))

    if opts.Verbose:
        for v in quiver:
            print('----')
            print(v.gps)
            print(v.classifier_data.segs)
            if hasattr(v.classifier_data, 'children'):
                print(v.classifier_data.children)
                for child in v.classifier_data.children:
                    print('   ', child.segs)
