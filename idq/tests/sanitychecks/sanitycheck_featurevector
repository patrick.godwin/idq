#!/usr/bin/env python

__usage__ = "sanitycheck_featurevector [--options] start end"
__doc__ = "basic testing for idq.features.FeatureVector"
__author__ = "reed.essick@ligo.org"

#-------------------------------------------------

import numpy as np

from idq import io
from idq import features

from optparse import OptionParser

#-------------------------------------------------

parser = OptionParser(usage=__usage__, description=__doc__)

parser.add_option('-v', '--verbose', default=False, action='store_true')
parser.add_option('-V', '--Verbose', default=False, action='store_true')

parser.add_option('', '--channels', default='channels.txt', type='string')

parser.add_option('', '--rootdir', default='/gds-l1/dmt/triggers/L-KW_TRIGGERS/', type='string')
parser.add_option('', '--basename', default='L-KW_TRIGGERS', type='string')

opts, args = parser.parse_args()
assert len(args)==2, 'please supply exactly 2 input arguments\n%s'%__usage__
start = float(args[0])
end = float(args[1])

opts.verbose |= opts.Verbose

gps = 0.5*(start+end) ### look at the mid point of the segment
window = 0.5*(end-start) ### window

label = None ### doesn't need to be labeled for these tests

time = 'time' ### column names
significance = 'significance'
default = -np.infty

#-------------------------------------------------

if opts.verbose:
    print('instantiating object')
kwm = io.KWMClassifierData(
    start,
    end,
    rootdir=opts.rootdir,
    basename=opts.basename,
    columns=['time', 'significance', 'frequency'],
)

#---

if opts.verbose:
    print('instantiating FeatureVector')
vector = features.FeatureVector(gps, None, kwm)

if opts.verbose:
    print('setting up vectorize arguments')
channels = io.path2channels(opts.channels)
select = features.SelectLoudest(
    default,
    window=window,
    time=time,
    significance=significance,
)

if opts.verbose:
    print('vectorizing')
d, n = vector.vectorize(
    channels, 
    select, 
)
