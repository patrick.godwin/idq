try:  # parse version
    from ._version import version as __version__
except ModuleNotFoundError:  # development mode
    __version__ = ""

import pluggy

hookimpl = pluggy.HookimplMarker("iDQ")
