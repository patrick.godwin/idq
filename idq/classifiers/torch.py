from skorch import NeuralNetClassifier
from skorch.callbacks import EarlyStopping

import torch.nn
import torch.optim

from .. import features
from .. import hookimpl
from .sklearn import SklearnModel, SupervisedSklearnClassifier


class PytorchModel(SklearnModel):
    def fit(self, data, labels, **kwargs):
        # cast to pytorch-compatible types
        data = data.astype("float32")
        labels = labels.astype("int64")
        super().fit(data, labels, **kwargs)

    def ranks(self, data):
        # cast to pytorch-compatible types
        data = data.astype("float32")
        return super().ranks(data)


class SupervisedTorchClassifier(SupervisedSklearnClassifier):
    """
    An abstract class to implement pytorch-based classifiers.
    """

    _flavor = "pytorch"

    def _create_model(self, dataset, channels):
        return PytorchModel(
            dataset.start,
            dataset.end,
            pipeline=self._create_pipeline(**self.kwargs.get("params", {})),
            channels=channels,
            downselector=features.DownselectLoudest(**self.kwargs),
            transformer=features.DeltaTimeTransformer(**self.kwargs),
            time=self.kwargs.get("time", features.DEFAULT_TIME_NAME),
            segs=dataset.segs,
            model_id=self._model_id,
        )

    def _create_pipeline(self, **kwargs):
        # set up commonly used quantities
        kwargs["classifier__module__num_features"] = self.num_features
        kwargs["classifier__module__num_channels"] = self.num_channels
        kwargs["classifier__module__num_columns"] = self.num_columns
        return super()._create_pipeline(**kwargs)

    def module(self):
        """
        Returns a pytorch neural network Module.
        """
        return NotImplementedError

    def criterion(self):
        """
        Returns a pytorch-compatible criterion.
        """
        return torch.nn.NLLLoss

    def optimizer(self):
        """
        Returns a pytorch Optimizer.
        """
        return torch.optim.Adam

    def classifier(self):
        """
        Creates a skorch neural network classifier.
        """
        classifier = NeuralNetClassifier(
            module=self.module(),
            callbacks=[
                ("early_stopping", EarlyStopping(patience=10)),
            ],
            criterion=self.criterion(),
            criterion__weight=torch.tensor(self.class_weights, dtype=torch.float),
            optimizer=self.optimizer(),
        )

        return [("classifier", classifier)]


# -------------------------------------------------
# plugin implementations


@hookimpl
def get_classifiers():
    return {}
