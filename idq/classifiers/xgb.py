from xgboost import XGBClassifier

from .. import hookimpl
from .sklearn import SupervisedSklearnClassifier


class XGBTree(SupervisedSklearnClassifier):
    """
    A gradient-boosted tree classifier based on xgboost.

    * `XGBoost Intro
    <https://xgboost.readthedocs.io/en/latest/tutorials/model.html>`_.

    * `XGBoost Hyperparameter Guide
    <https://xgboost.readthedocs.io/en/latest/parameter.html>`_.

    * `XGBoost API
    <https://xgboost.readthedocs.io/en/latest/python/python_api.html#module-xgboost.sklearn>`_.

    """

    _flavor = "xgb_tree"

    def classifier(self):
        """
        Creates an extreme gradient-boosted tree classifier.
        """
        classifier = XGBClassifier(eval_metric="logloss", use_label_encoder=False)
        return [("classifier", classifier)]


@hookimpl
def get_classifiers():
    return {
        "xgboost:xgb_tree": XGBTree,
    }
