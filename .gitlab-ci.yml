include:
  - project: computing/gitlab-ci-templates
    file:
      - python.yml

variables:
  DOCKER_DRIVER: overlay
  GIT_DEPTH: 1

stages:
  - build
  - test
  - deploy
  - docs
  - publish

# -- stages -------------------------------------

.in-tmpdir: &in-tmpdir
  before_script:
    - WORKING_DIRECTORY="$(mktemp -d)"
    - cd "${WORKING_DIRECTORY}"
  after_script:
    - cd "${CI_PROJECT_DIR}"
    - rm -rf "${WORKING_DIRECTORY}"

# Build source/binary distributions
dist:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/python/#.python:build
    - .python:build
  stage: build
  artifacts:
    paths:
      - '*.tar.gz'
      - '*.whl'

# Build Docker container for dependencies
dependencies:
  stage: build
  variables:
    GIT_STRATEGY: fetch
    IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:$CI_COMMIT_REF_NAME
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $IMAGE_TAG --file .gitlab/ci/Dockerfile.deps .
    - docker push $IMAGE_TAG
    - if [ "${CI_COMMIT_TAG:0:1}" = "v" ]; then docker tag $IMAGE_TAG ${IMAGE_TAG%:*}:latest; docker push ${IMAGE_TAG%:*}:latest; fi
  rules:
    - if: $CI_COMMIT_BRANCH
      changes:
        - .gitlab-ci.yml
        - .gitlab/ci/Dockerfile.deps
        - environment.yml
    - if: $CI_COMMIT_TAG
    - if: $CI_PIPELINE_SOURCE == "schedule"

# Run unit tests and coverage measurement
test:
  image: $CI_REGISTRY_IMAGE/dependencies:$CI_COMMIT_REF_NAME
  stage: test
  coverage: '/^TOTAL\s+.*\s+(\d+\.?\d*)%/'
  <<: *in-tmpdir
  script:
    - tar --strip-components 1 -xf ${CI_PROJECT_DIR}/*.tar.*
    - pip install .[test]
    - python -m pytest -vv --cov=idq --pyargs idq.tests --junit-xml=${CI_PROJECT_DIR}/junit.xml
  dependencies:
    - dist
  artifacts:
    reports:
      junit: junit.xml

# Linting: library
lint/lib:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/python/#.python:flake8
    - .python:flake8
  stage: test
  variables:
    FLAKE8_PLUGINS: flake8-black

# Build docker container for library
docker:
  stage: deploy
  script:
    - IMAGE_TAG=$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - |
      cat <<EOF > Dockerfile
      FROM $CI_REGISTRY_IMAGE/dependencies:$CI_COMMIT_REF_NAME
      ARG MAMBA_DOCKERFILE_ACTIVATE=1
      COPY *.tar.* .
      RUN pip install *.tar.*
      EOF
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
    - if [ "${CI_COMMIT_TAG:0:1}" = "v" ]; then docker tag $IMAGE_TAG ${IMAGE_TAG%:*}:latest; docker push ${IMAGE_TAG%:*}:latest; fi
  dependencies:
    - dist

# Generate documentation
doc:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  stage: docs
  <<: *in-tmpdir
  script:
    - tar --strip-components 1 -xf ${CI_PROJECT_DIR}/*.tar.*
    - pip install .[docs]
    - sphinx-build -b html doc/source html
    - mv html $CI_PROJECT_DIR
  artifacts:
    paths:
    - html
  dependencies:
    - docker
    - dist

# Publish docs
pages:
  stage: publish
  script:
    - mv html/ public/
  artifacts:
    paths:
      - public
  only:
    - main
  dependencies:
    - doc
