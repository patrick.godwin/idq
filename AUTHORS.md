## Authors

| Name                | Email                                 |
|---------------------|---------------------------------------|
| Reed Essick         | <reed.essick@ligo.org>                |
| Patrick Godwin      | <patrick.godwin@ligo.org>             |

## Maintainers

| Name                | Email                                 |
|---------------------|---------------------------------------|
| Patrick Godwin      | <patrick.godwin@ligo.org>             |

## Contributors

| Name                | Email                                 |
|---------------------|---------------------------------------|
| Rebecca Ewing       | <rebecca.ewing@ligo.org>              |
| Rachael Huxford     | <rachael.huxford@ligo.org>            |
| Geoffrey Mo         | <geoffrey.mo@ligo.org>                |
| Kyle Rose           | <rosek@legolas.kenyon.edu>            |
| Max Trevor          | <max.trevor@ligo.org>                 |
