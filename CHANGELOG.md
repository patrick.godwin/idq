# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.6.3] - 2024-01-04

### Added

- Add map coverage option to calibration accuracy plots
- Add compatibility with older (pre v0.5) data products

### Changed

- Make plotting feature importance plots optional in reports
- Remove efficiency and FAP columns from feature importance table
  to avoid confusion
- Modify how q-scan generation is done within timeseries plots
- Remove rank from timeseries plot and adjust plot ratio
- Add OK segments to timeseries plots
- Adjust logL threshold to 5 in reports
- Adjust ticks on logL timeseries for readability
- Plot timeseries as steps centered on individual points

### Fixed

- Improve performance of reports when feature importance plots
  are not generated
- Fix timeseries time axis label units
- Fix line break in feature importance plot
- Fix issue with combine_series where OK series were ignored

## [0.6.2] - 2023-05-11

### Fixed

- Fix issue with generation of YYMMDD-named reports

## [0.6.1] - 2023-05-09

### Added

- Add health check information to calibration maps
- Set IDQ_OK based on health information by default
- Add additional health checks based on livetime
- Allow setting IDQ_OK from configuration
- Expose KDE bandwidth factors as configuration option
- Expose YYMMDD-named reports for DQ summary page usage

### Changed

- Only show vetoes with nonzero livetime in feature importance table

### Fixed

- Fix shifting of replay segments for offline analysis
- Fix issue with order of optional condor batch arguments
- Fix issue in setting memory requests for condor batch jobs
- Avoid edge case in stream timeseries job when not all data products are
  available on startup

## [0.6.0] - 2023-04-21

### Added

- Add functionality to process replay data
- Add idq-smpull for production analyses
- Allow job-specific requirements for stream jobs
- Encode all data products with a data ID for provenance
- Ensure streaming jobs use data products with consistent data ID
- Add general Virgo support
- Add version information to report pages
- Add calibration accuracy plot to reports
- Allow OVL veto list to be saved as data quality flags
- Add tags to online DAG names for clarity

### Changed

- Make SNAX dataloader optional
- Use ezdag for condor DAG generation rather than custom approach
- Simplify preferred path implementation
- Allow missing channels in SNAX dataloader
- Kafka SNAX dataloader: port -> url
- Allow job-specific feature configuration for timeseries
- Use gpstime library for GPS functionality
- Collect metrics with scald rather than idq-monitor
- Switch to ujson for decoding SNAX features
- Have dataloaders raise NoDataError when it applies
- Refactor CLI to avoid duplication
- Decouple stream calibration and evaluation jobs

### Removed

- Remove Kafka reporter from being registered
- Remove unused condor batch programs
- Remove idq-monitor in favor of scald
- Remove start/end time arguments in stream DAG generation
- Remove written logs, write to stderr instead

### Fixed

- Fix issue in discovering data products
- Fix various issues in streaming jobs
- Respect ignore_segdb flag in load_record_segments

## [0.5.0] - 2022-06-10

### Added

- Limit parameter space of triggers in aux channels
- Allow union of flags for DQSegDB queries

### Changed

- Replace trigger reading / discovery w/ gwpy + gwtrigfind
- Switch to ligo-segments for all segments logic
- Simplify configuration handling for usability
- Report/summary page overhaul w/ ligo-scald
- Use all data for background collection
- OVL: force KDE bandwidth lower limit

### Fixed

- OVL: Classifier changes + bug fixes

## [0.4.0] - 2021-03-19

### Added

- Handle unreadable Omicron files gracefully

### Changed

- Migrate from dqsegdb to dqsegdb2
- Switch configuration file format from INI to TOML to allow for native types,
  validation without execution
- Switch to plugin-based model for classifiers and I/O
- Improve reliability of batch jobs run under condor
- GPS time argument sanity checks in various programs

### Removed

- Drop python 2 support
- Remove exposing avg_rate_threshold low-level option
- Purge any remaining use of eval() in favor of safer methods
- Remove custom log_uniform distribution in favor of scipy.stats.reciprocal

### Fixed

- Fix issue in binning when bin edges fall on non-integer seconds
- Fix edge case bug in Omicron trigger backend
- matplotlib 3.x compatibility fixes

## [0.3.1] - 2020-02-21

### Fixed

- Fix issues with storing clean segments for online rate estimation when using
  the `livetime` approach.
- Fix edge case when retrieving paths in cache for preferred data products when
  path is exactly `byte_stride` characters.
- Fix issue in generators for python 3.7, where raising `StopIteration` raises
  an error when generating timeseries online.

## [0.3.0] - 2020-01-08

### Added

- Add python 3 support
- Add livetime-based estimation for calibration, impacting p(glitch) scaling

### Changed

- Modify file access patterns for storing data products, mitigates NFS issues
  seen in production
- Performance improvements in report/plot generation
- Finer control of online strides, allowing reports to be generated to align
  with daily DQ summary pages

## [0.2.0] - 2019-07-15

### Added

- Add a datasource for Kleine-Welle triggers in HDF5 format
- Add idq-kwm2hdf5 to make HDF5 KW triggers from ascii format

### Changed

- Expose a cleaner interface for specifying neural-network layers when using
  Keras classifiers

### Fixed

- Fixes with batch jobs relating to tracking model provenance
- Fix many issues with report generation when running batch jobs, particularly
  when mixing up the training/evaluation sets between chunks.
- Fix crash when running Keras-based classifiers

[unreleased]: https://git.ligo.org/lscsoft/iDQ/-/compare/v0.6.3...main
[0.6.3]: https://git.ligo.org/lscsoft/iDQ/-/tags/v0.6.3
[0.6.2]: https://git.ligo.org/lscsoft/iDQ/-/tags/v0.6.2
[0.6.1]: https://git.ligo.org/lscsoft/iDQ/-/tags/v0.6.1
[0.6.0]: https://git.ligo.org/lscsoft/iDQ/-/tags/v0.6.0
[0.5.0]: https://git.ligo.org/lscsoft/iDQ/-/tags/v0.5.0
[0.4.0]: https://git.ligo.org/lscsoft/iDQ/-/tags/v0.4.0
[0.3.1]: https://git.ligo.org/lscsoft/iDQ/-/tags/v0.3.1
[0.3.0]: https://git.ligo.org/lscsoft/iDQ/-/tags/v0.3.0
[0.2.0]: https://git.ligo.org/lscsoft/iDQ/-/tags/v0.2.0
